import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { QuizzPage } from '../quizz/quizz.page';
import { QuizzPageModule } from '../quizz/quizz.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    QuizzPageModule

  ],
  entryComponents: [QuizzPage],
  declarations: [TabsPage]
})
export class TabsPageModule { }
