import { Component, ViewChild } from '@angular/core';
import { IonTabs, Events } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  @ViewChild('tabs', { static: true }) tabs: IonTabs;

  constructor(private events: Events) {
    this.events.subscribe('tabChange', (name) => {
      console.log("name", name)
      this.tabChange(name);
    })
  }

  ngOnInit() {


  }

  tabChange(name) {
    console.log(this.tabs);
    this.tabs.select(name);
  }

}
