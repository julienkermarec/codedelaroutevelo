import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoursSlidesPage } from './cours-slides.page';

describe('CoursSlidesPage', () => {
  let component: CoursSlidesPage;
  let fixture: ComponentFixture<CoursSlidesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CoursSlidesPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoursSlidesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
