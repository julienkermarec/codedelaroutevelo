import { Component, ViewChild } from '@angular/core';
import { NavController, Events, IonTabs, ModalController } from '@ionic/angular';
import { QuizzPage } from '../quizz/quizz.page';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})


export class Tab1Page {


  constructor(
    private events: Events,
    private modalController: ModalController
  ) {
  }

  goTab2() {
    this.events.publish('tabChange', 'tab2');
  }


  async goQuizz() {
    const modal = await this.modalController.create({
      component: QuizzPage,
      componentProps: {
      }
    });

    modal.onDidDismiss().then((data) => {
      if (data !== null) {
        console.log('data', data);
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
    return await modal.present();
  }
}
