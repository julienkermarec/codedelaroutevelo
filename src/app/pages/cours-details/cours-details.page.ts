import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoriesService } from 'src/app/services/categories.service';
import { CoursService } from 'src/app/services/cours.service';
import { ModalController } from '@ionic/angular';
import { CoursSlidesPage } from '../cours-slides/cours-slides.page';

@Component({
  selector: 'app-cours-details',
  templateUrl: './cours-details.page.html',
  styleUrls: ['./cours-details.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CoursDetailsPage implements OnInit {
  categorie_id: any;
  categorie: any;
  cours: any;
  showMore = false;
  checkmark = -1;
  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private coursService: CoursService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.getData();
  }

  toogleMore() {
    this.showMore = !this.showMore;
  }


  async presentModal(cour) {
    const modal = await this.modalController.create({
      component: CoursSlidesPage,
      componentProps: {
        cour: cour
      },
      cssClass: "modal-fullscreen"
    });

    modal.onDidDismiss().then((data) => {
      if (data !== null) {
        console.log('data', data);
        if (data.data == "finished") {
          this.checkmark = 0;
        }
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
    return await modal.present();
  }

  getData() {
    this.route.data.subscribe(routeData => {
      console.log("routeData", routeData);
      let data = routeData['data'];
      if (data) {
        console.log("DetailsPage getData", data);
        this.categorie_id = data;
        this.refresh();
      }
    })
  }

  async refresh() {
    this.categorie = await this.categoriesService.getById(this.categorie_id);
    this.cours = await this.coursService.getAllByCategorie(this.categorie_id);
  }
}
