import { Component } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  categories = [];

  constructor(
    private categoriesService: CategoriesService,
    private router: Router
  ) { }

  ngOnInit() {

  }
  ionViewDidEnter() {
    this.refresh();
  }

  async refresh() {
    this.categories = await this.categoriesService.getAll();

    console.log("this.categories", this.categories);
  }

  goCoursDetails(item) {
    console.log("item", item.id)
    this.router.navigate(["/tabs/tab2/cours-details", item.id]);
  }
}
